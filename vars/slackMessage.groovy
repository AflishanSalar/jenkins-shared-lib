#!/usr/bin/env groovy

def call(String type, String message) {
    switch (type) {
        case 'GOOD':
            slackSend color: "good", message: message
            break
        case 'WARNING':
            slackSend color: "warning", message: message
            break
        case 'DANGER':
            slackSend color: "danger", message: message
            break
        default:
            slackSend color: "#439FE0", message: message
    }
}
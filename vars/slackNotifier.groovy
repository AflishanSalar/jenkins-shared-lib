#!/usr/bin/env groovy

def call(String buildResult) {
    if (buildResult == "STARTED") {
        slackSend color: "#439FE0", message: "Build Started: ${env.JOB_NAME} ${env.BUILD_NUMBER} GIT INFO : ${env.GIT_COMMITTER_NAME} ,  ${env.GIT_COMMITTER_EMAIL} on node : ${env.NODE_NAME}"
    } else if (buildResult == "SUCCESS") {
        slackSend color: "good", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was successful"
    } else if (buildResult == "FAILURE") {
        slackSend color: "danger", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was failed"
    } else if (buildResult == "UNSTABLE") {
        slackSend color: "warning", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was unstable"
    } else {
        slackSend color: "danger", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} its resulat was unclear"
    }
}